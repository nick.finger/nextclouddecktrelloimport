from dotenv import load_dotenv
import os
import json
from deck.api import NextCloudDeckAPI

def main():
    load_dotenv()
    URL = os.getenv("URL")
    TRELLO_JSON_PATH = os.getenv("TRELLO_JSON_PATH")
    TRELLO_BOARD_ATTACHEMENTS_PATH = os.getenv("TRELLO_BOARD_ATTACHEMENTS_PATH")

    deck = NextCloudDeckAPI(
        URL,
        (os.getenv("USERNAME"), os.getenv("PASSWORD")),
        ssl_verify=True,
        deck_api_path="/apps/deck/api/v1.0/boards"
        )

    with open(TRELLO_JSON_PATH, "r") as read_file:
        trello_board = json.load(read_file)
    
    with open("color_map.json", "r") as read_file:
        colors = json.load(read_file)

    #new_board = deck.create_board(trello_board['name'])
    print(deck.get_boards())
    #for label in trello_board['labels']:
       # deck.create_label(new_board.id, label['name'], colors[label["color"]])



def create_labels(board):
    print("Hello World")

if __name__ == "__main__":
    main()
